<?php
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor; 
use SilverStripe\ORM\DataObject;

class Pengaduan extends DataObject{
    private static $db = [
        "nama"=>"Varchar",
        "alamat"=>"Varchar",
        "email"=>"Varchar",
        "tanggal"=>"Date",
        "isi"=>"Text"
    ];

    private static $has_many = [
        "tanggapan"=>Tanggapan::class
    ];


    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', GridField::create(
            'Tanggapan',
            'tanggapan Pada Laman Ini',
            $this->tanggapan(),
            GridFieldConfig_RecordEditor::create()
        ));
        return $fields;
    }

    private static $summary_fields = [
        'nama' => 'Pengirim',
        'isi' => 'Isi Pengaduan',
        'tanggal.Nice' =>Tanggal
     ];

}