<?php
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Control\HTTPResponse_Exception;
use SilverStripe\ORM\DataObject;

// use Pengaduan;

class PengaduanPage extends Page{

}

class PengaduanPageController extends PageController{

    private static $allowed_actions = [
        "submit",
        "addTanggapan",
        "limitTanggapan"
    ];

    public function index(HTTPRequest $request){
        $session = $this->MySession();
        $data["Title"] = "Pengaduan";

        // $pengaduan = new Pengaduan();
        $data["Pengaduan"] = Pengaduan::get()->sort("ID", "DESC");

        if($session->get('msg')){
            $data["msg"] = true;
            $session->set('msg', false);
        }else{
            $data["msg"] = false;
        }
        return $data;
    }

    public function limitTanggapan($n){
        return Tanggapan::get()->limit($n);
    }
    
    public function submit($data){
        $session = $this->MySession();

        $pengaduan = new Pengaduan();
        $pengaduan->nama = $data["nama"];
        $pengaduan->alamat = $data["alamat"];
        $pengaduan->email = $data["email"];
        $pengaduan->isi = $data["isi"];
        $pengaduan->tanggal = date("Y-m-d");
        $id = $pengaduan->write();
        if($id){
            $session->set('msg', true);
        }else{
            $session->set('msg', false);
        }
        return $this->redirect("pengaduan/");
    }

    public function MySession()
    {
        return $this->getRequest()->getSession();
    }

    public function addTanggapan($data)
    {
        $res = [];
        $tanggapan = new Tanggapan();
        $tanggapan->pengirim = $data["pengirim"];
        $tanggapan->isi = $data["isi"];
        $tanggapan->tanggal = date("Y-m-d");
        $tanggapan->pengaduanID = $data["pengaduanID"];
        $id = $tanggapan->write();
        if($id){
            $res["status"] = true;
        }else{
            $res["status"] = false;
        };
        $res["data"] = ["ID"=>$id ,"pengirim"=>$data["pengirim"], "isi"=>$data["isi"], "tanggal"=>date("M d Y")];
        return (new HTTPResponse(json_encode($res)))->addHeader('Content-Type', 'application/json');
    }

}