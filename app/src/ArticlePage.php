<?php
    use SilverStripe\Forms\DateField;
    use SilverStripe\Forms\TextField;

	class ArticlePage extends Page {
        private static $db = array(
			'Tanggal' => 'Date',
			'Sumber' => 'Text'
        );
        
        public function getCMSFields() {
            $fields = parent::getCMSFields();
            $fields->addFieldToTab('Root.Main', DateField::create('Tanggal','Date of article'), 'Content');
            $fields->addFieldToTab('Root.Main', TextField::create('Sumber'), 'Content');
            return $fields;
        }
	}
	class ArticlePageController extends PageController {
	}
	 