<?php
use SilverStripe\Admin\ModelAdmin;

class TanggapanAdmin extends ModelAdmin{
    private static $managed_models = [
        'Pengaduan',
    ];
    private static $url_segment = 'pengaduan';
    private static $menu_title = 'Pengaduan Masyrakat';

    private static $searchable_fields = [
        "nama"
    ];

}