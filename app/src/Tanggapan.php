<?php
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DateField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataObject;

class Tanggapan extends DataObject{
    private static $db = [
        "pengirim"=>"Varchar",
        "isi"=>"Text",
        "tanggal"=>"Date"
    ];

    private static $has_one = [
        "pengaduan" => Pengaduan::class
    ];

    public function getCMSFields()
    {
        $fields = FieldList::create(
            TextField::create('pengirim'),
            TextareaField::create('isi'),
            DateField::create('tanggal')
        );
        return $fields;
    }

    private static $summary_fields = [
        'pengirim' => 'Pengirim',
        'isi' => 'Tanggapan',
        'tanggal.Nice' => 'Tanggal'
     ];
}